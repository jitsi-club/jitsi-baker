#!/bin/bash

# SPDX-License-Identifier: GPL-3.0-only
# SPDX-FileCopyrightText: 2020 Enrico Ferraris <dev@ferrarislegal.it>

# TODO: lines 57, 353, 376, 428, 430 (backup cfg w/ date + unlimited startAudioMuted / startVideoMuted)

red=$(tput setaf 1)
green=$(tput setaf 76)
normal=$(tput sgr0)
bold=$(tput bold)
underline=$(tput sgr 0 1)

confout="jc-config_output.log"
conferr="jc-config_error.log"
jitsicfgfile="/etc/jitsi/meet/$(hostname -f)-config.js"
prosodycfgfile="/etc/prosody/conf.avail/$(hostname -f).cfg.lua"
languagesfile="/usr/share/jitsi-meet/lang/languages.json"

i_ok() { printf "${green}✔${normal}\n"; }
i_ko() { printf "${red}✖${normal}\n\nAn error occurred, check logs.\n"; }

function check_i {
  if [ $? -eq 0 ]; then
    i_ok
  else
    i_ko; read
    config_menu
  fi
}

function gitter {
  if [ -x "$(command -v git)" ]; then
    printf "Checking git..."
    check_i
  else
    printf "Installing git..."
    apt-get install -y git 1>>$confout 2>>$conferr
    check_i
  fi
  if [ -d "jitsicfg" ]; then
    printf "Updating Jitsicfg git repo...\n"
    cd jitsicfg
    git pull
    cd ..
  else
    printf "Cloning Jitsicfg git repo...\n"
    git clone https://gitlab.com/jitsi-club/jitsicfg.git 1>>$confout 2>>$conferr
    check_i
  fi
  cp jitsicfg/jitsicfg /usr/local/bin
  chmod +x /usr/local/bin/jitsicfg
}

function backupcfg {
  # TODO: backup cfg files every time, adding a date tag at the end?
  if [[ ! -f $jitsicfgfile".orig" ]]; then
    printf "Backing up jitsi config file...\n"
    cp $jitsicfgfile $jitsicfgfile".orig"
    check_i
  fi
  if [[ ! -f $prosodycfgfile".orig" ]]; then
    printf "Backing up prosody config file...\n"
    cp $prosodycfgfile $prosodycfgfile".orig"
    check_i
  fi
}

function back_menu {
  service jicofo restart
  service jitsi-videobridge2 restart
  printf "\nBack to menu. "; read -n1
  config_menu
}

function congrats {
  clear
  printf "Congratulations! Your jitsi-meet is now optimally baked!\n\n Back to menu. "; read -n1
  config_menu
}


function option_enableauth {
  clear
  printf "${bold}1. Enable authentication${normal}\n\n"
  printf "Setting hashed authentication..."
  jitsicfg prosody::vhost_setfield \
    $prosodycfgfile $(hostname -f) \
    authentication '"internal_hashed"' 1>>$confout 2>>$conferr
  check_i
  printf "Creating Prosody virtualhost for guests..."
  jitsicfg prosody::addvhost \
    $prosodycfgfile guest.$(hostname -f) 1>>$confout 2>>$conferr
  check_i
  printf "Setting new virtualhost as anonymous..."
  jitsicfg prosody::vhost_setfield \
    $prosodycfgfile guest.$(hostname -f) \
    authentication '"anonymous"' 1>>$confout 2>>$conferr
  check_i
  printf "Finishing setup..."
  jitsicfg prosody::vhost_setfield \
    $prosodycfgfile guest.$(hostname -f) \
    c2s_require_encryption false 1>>$confout 2>>$conferr
  check_i
  set="'guest.$(hostname -f)'"
  printf "Enabling anonymous domain in Jitsi config..."
  jitsicfg jitsi::enablesetting \
    $jitsicfgfile anonymousdomain 1>>$confout 2>>$conferr
  check_i
  printf "Setting address..."
  jitsicfg jitsi::setsetting \
    $jitsicfgfile anonymousdomain $set 1>>$confout 2>>$conferr
  check_i
  printf "Configuring Jicofo..."
  if [ ! $(grep org.jitsi.jicofo.auth.URL=XMPP:$(hostname -f) /etc/jitsi/jicofo/sip-communicator.properties) ]; then
    echo "org.jitsi.jicofo.auth.URL=XMPP:$(hostname -f)" >> /etc/jitsi/jicofo/sip-communicator.properties
    check_i
  fi
  printf "Importing Let's Encrypt Certificates to Prosody..."
  prosodyctl --root cert import /etc/letsencrypt/live 1>>$confout 2>>$conferr
  check_i
  printf "Setting up renewal post-hook..."
  cat <<EOT > /etc/letsencrypt/renewal-hooks/post/00-prosody-auth.sh
#!/bin/bash
prosodyctl --root cert import /etc/letsencrypt/live
service prosody restart
EOT
  check_i
  chmod +x /etc/letsencrypt/renewal-hooks/post/00-prosody-auth.sh 1>>$confout 2>>$conferr
  printf "\nGo on and create your first moderator user account..."; read -n1
  option_adduser
}

function option_adduser {
  clear
  printf "${bold}Create moderator user account${normal}\n\n"
  printf "Username: "; read modusername
  prosodyctl adduser $modusername@$(hostname -f)
  printf "\nUser $modusername created..."
  check_i
}

function option_deluser {
  clear
    printf "${bold}Delete moderator user account${normal}\n\n"
    printf "Username: "; read modusername
    prosodyctl deluser $modusername@$(hostname -f)
    printf "\nUser $modusername deleted..."
    check_i
}

function option_pswuser {
  clear
    printf "${bold}Change moderator user password${normal}\n\n"
    printf "Username: "; read modusername
    prosodyctl passwd $modusername@$(hostname -f)
    printf "\nUser $modusername password changed..."
    check_i
}

function option_fail2ban {
  clear
  printf "${bold}Enable Prosody Fail2Ban module${normal}\n\n"
  printf "Installing Mercurial..."
  apt-get install -y mercurial 1>>$confout 2>>$conferr
  check_i
  if [[ ! -d ./prosody-modules ]]; then
    printf "Cloning Prosody modules..."
    hg clone https://hg.prosody.im/prosody-modules/ prosody-modules 1>>$confout 2>>$conferr
    check_i
  else
    printf "Updating Prosody modules..."
    cd prosody-modules
    hg pull https://hg.prosody.im/prosody-modules/ 1>>$confout 2>>$conferr
    check_i
    cd ..
  fi
  printf "Copying Log Auth module..."
  cp prosody-modules/mod_log_auth/mod_log_auth.lua /usr/lib/prosody/modules/ 1>>$confout 2>>$conferr
  check_i
  printf "Patching Prosody configuration..."
  jitsicfg prosody::vhost_additem2section \
    $prosodycfgfile $(hostname -f) \
    modules_enabled '"log_auth"'
  check_i
  printf "Restarting Prosody..."
  service prosody restart
  check_i
  printf "Configuring Fail2Ban filter..."
  cat <<EOT > /etc/fail2ban/filter.d/prosody-auth.conf
# /etc/fail2ban/filter.d/prosody-auth.conf
# Fail2Ban configuration file for prosody authentication
[Definition]
failregex = Failed authentication attempt \(not-authorized\) for user .* from IP: <HOST>
ignoreregex =
EOT
  check_i
  printf "Configuring Fail2Ban jail..."
  jailfile="/etc/fail2ban/jail.local"
  if [[ ! -f "Sjailfile" || ! $(grep "[prosody]" "$jailfile" ) ]]; then
    cat <<EOT >> "$jailfile"
[prosody]
enabled = true
iptables-multiport[name="prosody", port="443,5222,5269"]
port    = 443,5222,5269
filter  = prosody-auth
logpath = /var/log/prosody/prosody*.log
maxretry = 5
EOT
  fi
  check_i
  printf "Restarting Fail2Ban..."
  service fail2ban restart
  check_i
}

function option_lang {
  clear
  printf "${bold}Change default language${normal}\n\n"
  printf "Enter the new value, or press ENTER for the default.\n\n"
  printf "Insert language code [$1]: "; read newlang
  [[ $newlang == "" ]] && newlang=$1
  allang=($(sed 's/:.*$//;s/^[ \t]*//;s/"//g;1d;$d' $languagesfile))
  until [[ " ${allang[@]} " =~ " $newlang " ]]; do
    printf "Language not available.\n"
    printf "Insert language code [$1]: "; read newlang
    [[ $newlang == "" ]] && newlang=$1
  done
  set="'$newlang'"
  printf "\nEnabling setting..."
  jitsicfg jitsi::enablesetting \
  $jitsicfgfile defaultLanguage
  check_i
  printf "Applying setting..."
  jitsicfg jitsi::setsetting \
  $jitsicfgfile defaultLanguage $set
  check_i
}

function option_displayname {
  [[ $1 == "true" ]] && setdisplayname="false" || setdisplayname="true"
  clear
  printf "${bold}Enbale/Disable display name dialog${normal}\n\n"
  printf "Enabling setting..."
  jitsicfg jitsi::enablesetting \
  $jitsicfgfile requireDisplayName
  check_i
  printf "Applying setting..."
  jitsicfg jitsi::setsetting \
  $jitsicfgfile requireDisplayName $setdisplayname
  check_i
}

function option_simulcast {
  [[ $1 == "true" ]] && setsimulcast="false" || setsimulcast="true"
  clear
  printf "${bold}Enable/Disable H264 codec${normal}\n\n"
  printf "Enabling setting..."
  jitsicfg jitsi::enablesetting \
  $jitsicfgfile disableSimulcast
  check_i
  printf "Applying setting..."
  jitsicfg jitsi::setsetting \
  $jitsicfgfile disableSimulcast $setsimulcast
  check_i
}


function option_enableH264 {
  [[ $1 == "true" ]] && seth264="false" || seth264="true"
  clear
  printf "${bold}Enable/Disable H264 codec${normal}\n\n"
  printf "Enabling setting..."
  jitsicfg jitsi::enablesetting \
  $jitsicfgfile preferH264
  check_i
  printf "Applying setting..."
  jitsicfg jitsi::setsetting \
  $jitsicfgfile preferH264 $seth264
  check_i
}

function option_layersusp {
  [[ $1 == "true" ]] && setlayersusp="false" || setlayersusp="true"
  clear
  printf "${bold}Enable/Disable layer suspension${normal}\n\n"
  printf "Enabling setting..."
  jitsicfg jitsi::enablesetting \
  $jitsicfgfile enableLayerSuspension
  check_i
  printf "Applying setting..."
  jitsicfg jitsi::setsetting \
  $jitsicfgfile enableLayerSuspension $setlayersusp
  check_i
}

function option_resolution {
  clear
  printf "${bold}Set video resolution${normal}\n\n"
  if [[ -z "$2" ]]; then
    printf "Enter the new value, or press ENTER for the default.\n\n"
    printf "Max resolution [$1]: "; read modresolution
  else
    modresolution=$2
  fi
  [[ -z "$modresolution" ]] && modresolution=$1
  until [[ $modresolution =~ [0-9] ]]; do
    printf "Value is not valid.\n"
    printf "Max resolution [$1]: "; read modresolution
   [[ -z "$modresolution" ]] && modresolution=$1
  done
  printf "\nEnabling setting..."
  jitsicfg jitsi::enablesetting \
  $jitsicfgfile resolution
  jitsicfg jitsi::enablesection \
  $jitsicfgfile constraints
  check_i
  printf "Applying setting..."
  jitsicfg jitsi::setsetting \
  $jitsicfgfile resolution $modresolution
  jitsicfg jitsi::setsectionsetting \
  $jitsicfgfile constraints ideal $modresolution
  jitsicfg jitsi::setsectionsetting \
  $jitsicfgfile constraints max $modresolution
  check_i
}

function option_lastn {
  clear
  printf "${bold}Set number of video feeds (LastN)${normal}\n\n"
  if [[ -z "$2" ]]; then
    printf "Enter the new value, or press ENTER for the default.\n\n"
    printf "Last N [$1]: "; read modlastn
  else
    modlastn=$2
  fi
  [[ -z "$modlastn" ]] && modlastn=$1
  until [[ $modlastn =~ [0-9] ]]; do
    printf "Value is not valid.\n"
    printf "Max resolution [$1]: "; read modlastn
   [[ -z "$modlastn" ]] && modlastn=$1
  done
  printf "\nEnabling setting..."
  jitsicfg jitsi::enablesetting \
  $jitsicfgfile channelLastN
  check_i
  printf "Applying setting..."
  jitsicfg jitsi::setsetting \
  $jitsicfgfile channelLastN $modlastn
  check_i
}

function option_audioonly {
  [[ $1 == "true" ]] && setaudioonly="false" || setaudioonly="true"
  clear
  printf "${bold}Enable/Disable webcams at start${normal}\n\n"
  printf "Enabling setting..."
  jitsicfg jitsi::enablesetting \
  $jitsicfgfile startAudioOnly
  check_i
  printf "Applying setting..."
  jitsicfg jitsi::setsetting \
  $jitsicfgfile startAudioOnly $setaudioonly
  check_i
}

function option_audiomuted {
  clear
  printf "${bold}Enable/Disable audio mute after the Nth participant at start${normal}\n\n"
  printf "Enter the new value, or press ENTER for the default.\n\n"
  printf "Audio muted after [$1]: "; read modaudiomuted
  # TODO: check if 0 or -1 = unlimited
  [[ -z "$modaudiomuted" ]] && modaudiomuted=$1
  until [[ $modaudiomuted =~ [0-9] ]]; do
    printf "Value is not valid.\n"
    printf "Max resolution [$1]: "; read modaudiomuted
   [[ -z "$modaudiomuted" ]] && modaudiomuted=$1
  done
  printf "Enabling setting..."
  jitsicfg jitsi::enablesetting \
  $jitsicfgfile startAudioMuted
  check_i
  printf "Applying setting..."
  jitsicfg jitsi::setsetting \
  $jitsicfgfile startAudioMuted $modaudiomuted
  check_i
}

function option_videomuted {
  clear
  printf "${bold}Enable/Disable video mute after the Nth participant at start${normal}\n\n"
  printf "Enter the new value, or press ENTER for the default.\n\n"
  printf "Video muted after [$1]: "; read modvideomuted
  # TODO: check if 0 or -1 = unlimited
  [[ -z "$modvideomuted" ]] && modvideomuted=$1
  until [[ $modvideomuted =~ [0-9] ]]; do
    printf "Value is not valid.\n"
    printf "Max resolution [$1]: "; read modvideomuted
   [[ -z "$modvideomuted" ]] && modvideomuted=$1
  done
  printf "Enabling setting..."
  jitsicfg jitsi::enablesetting \
  $jitsicfgfile startVideoMuted
  check_i
  printf "Applying setting..."
  jitsicfg jitsi::setsetting \
  $jitsicfgfile startVideoMuted $modvideomuted
  check_i
}

function option_0 {
  option_enableauth
  option_fail2ban
  option_lang $deflang
  option_displayname
  option_enableH264
  option_layersusp
  option_resolution $defresolution 480
  option_lastn $deflastn 4
}

function config_menu {
clear
printf "Loading..."
deflang=$(jitsicfg jitsi::getsetting $jitsicfgfile defaultLanguage | sed "s/'//g")
defdisplayname=$(jitsicfg jitsi::getsetting $jitsicfgfile requireDisplayName)
defh264=$(jitsicfg jitsi::getsetting $jitsicfgfile preferH264)
defsimulcast=$(jitsicfg jitsi::getsetting $jitsicfgfile disableSimulcast)
deflayersusp=$(jitsicfg jitsi::getsetting $jitsicfgfile enableLayerSuspension)
defresolution=$(jitsicfg jitsi::getsectionsetting $jitsicfgfile constraints max)
deflastn=$(jitsicfg jitsi::getsetting $jitsicfgfile channelLastN)
defaudioonly=$(jitsicfg jitsi::getsetting $jitsicfgfile startAudioOnly)
defaudiomuted=$(jitsicfg jitsi::getsetting $jitsicfgfile startAudioMuted)
defvideomuted=$(jitsicfg jitsi::getsetting $jitsicfgfile startVideoMuted)

[[ $deflang == "" ]] && deflang="en"
[[ $defdisplayname == "true" ]] && statdisplayname="ON" || statdisplayname="OFF"
[[ $defh264 == "true" ]] && stath264="ON" || stath264="OFF"
[[ $defsimulcast == "true" ]] && statsimulcast="OFF" || statsimulcast="ON"
[[ $deflayersusp == "true" ]] && statlayersusp="ON" || statlayersusp="OFF"
[[ $defresolution == "" ]] && defresolution="720" && statresolution="720" || statresolution="$defresolution"
[[ $deflastn == "-1" ]] && statlastn="OFF" || statlastn="N: $deflastn"
[[ $defaudioonly == "true" ]] && stataudioonly="OFF" || stataudioonly="ON"
[[ $defaudiomuted == "" ]] && stataudiomuted="OFF" || stataudiomuted="N: $defaudiomuted"
# TODO: check 0 or -1 = unlimited
[[ $defvideomuted == "" ]] && statvideomuted="OFF" || statvideomuted="N: $defvideomuted"
# TODO: check 0 or -1 = unlimited
i=0
clear
printf "${bold}Welcome to Jisti-Meet configurator by Jitsi Club${normal}\n\n"
printf " $i. Apply *recommended settings*\n\n"
printf "${underline}Prosody section${normal}\n"
((i++)); printf " $i. Enable authentication\n"; id_enableauth=$i
((i++)); printf " $i. Create moderator user account\n"; id_adduser=$i
((i++)); printf " $i. Delete moderator user account\n"; id_deluser=$i
((i++)); printf " $i. Change moderator user password\n"; id_pswuser=$i
((i++)); printf " $i. Enable Prosody Fail2Ban module\n\n"; id_fail2ban=$i
printf "${underline}Jitsi-meet section${normal}\n"
((i++)); printf " $i. Set default language [${green}$deflang${normal}]\n"; id_lang=$i
((i++)); printf " $i. Enbale/Disable display name dialog [${green}$statdisplayname${normal}]\n"; id_displayname=$i
((i++)); printf " $i. Enable/Disable Simulcast [${green}$statsimulcast${normal}]\n"; id_simulcast=$i
((i++)); printf "$i. Enable/Disable H264 codec [${green}$stath264${normal}]\n"; id_enableh264=$i
((i++)); printf "$i. Enable/Disable layer suspension [${green}$statlayersusp${normal}]\n"; id_layersusp=$i
((i++)); printf "$i. Set video resolution [${green}$statresolution${normal}]\n"; id_resolution=$i
((i++)); printf "$i. Set number of video feeds (LastN) [${green}$statlastn${normal}]\n"; id_lastn=$i
((i++)); printf "$i. Enable/Disable webcams at start [${green}$stataudioonly${normal}]\n"; id_audioonly=$i
((i++)); printf "$i. Enable/Disable audio mute after the Nth participant at start [${green}$stataudiomuted${normal}]\n"; id_audiomuted=$i
((i++)); printf "$i. Enable/Disable video mute after the Nth participant at start [${green}$statvideomuted${normal}]\n\n"; id_videomuted=$i
printf "${underline}Privacy section${normal}\n"
printf "...\n\n"
printf "➜ more configurations to come.\n\n"
printf "Choose an option [Q to exit]: "; read choice

until [[ $choice =~ [0-9Qq] ]]; do
      printf "\nWrong input, choose an option: "; read choice
done

case $choice in
  0)
    option_0; congrats
    ;;
  $id_enableauth)
    option_enableauth; option_adduser ; back_menu
    ;;
  $id_adduser)
    option_adduser; back_menu
    ;;
  $id_deluser)
    option_deluser; back_menu
    ;;
  $id_pswuser)
    option_pswuser; back_menu
    ;;
  $id_fail2ban)
    option_fail2ban; back_menu
    ;;
  $id_lang)
    option_lang $deflang; back_menu
    ;;
  $id_displayname)
    option_displayname $defdisplayname; back_menu
    ;;
  $id_simulcast)
    option_simulcast $defsimulcast; back_menu
    ;;
  $id_enableh264)
    option_enableH264 $defh264; back_menu
    ;;
  $id_layersusp)
    option_layersusp $deflayersusp; back_menu
    ;;
  $id_resolution)
    option_resolution $defresolution; back_menu
    ;;
  $id_lastn)
    option_lastn $deflastn; back_menu
    ;;
  $id_audioonly)
    option_audioonly $defaudioonly; back_menu
    ;;
  $id_audiomuted)
    option_audiomuted $defaudiomuted; back_menu
    ;;
  $id_videomuted)
    option_videomuted $defvideomuted; back_menu
    ;;
  [Qq])
    clear; printf "Have a nice day!\n\nThe Jitsi Club.\n\n"; exit
    ;;
  *)
    printf "\nOption not available. Back to menu. "; read -n1
    config_menu
    ;;
esac

exit
}

clear

if [[ $EUID == 0 ]]; then
  > $confout
  > $conferr
  gitter
  backupcfg
  config_menu
else
  printf "Run JitsiClubConfigurator as sudoer.\n"
  printf "Press any key to exit. "; read -n1
fi
