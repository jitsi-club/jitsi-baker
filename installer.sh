#!/bin/bash

# SPDX-License-Identifier: GPL-3.0-only
# SPDX-FileCopyrightText: 2020 Enrico Ferraris <dev@ferrarislegal.it>

red=$(tput setaf 1)
green=$(tput setaf 76)
normal=$(tput sgr0)
bold=$(tput bold)
underline=$(tput sgr 0 1)

instout="jc-install_output.log"
insterr="jc-install_error.log"

myname=$0
domain=$1
email=$2
hostn=$(sed 's/\..*//' <<< $domain)

i_ok() { printf "${green}✔${normal}\n"; }
i_ko() { printf "${red}✖${normal}\n...exiting, check logs"; }

function check_i {
  if [ $? -eq 0 ]; then
    i_ok
  else
    i_ko; read
    exit
  fi
}

function instruction {
printf "Usage: sudo $1 meet.example.com mail@example.com\n\n"
printf "       ➜ input your actual domain and e-mail address.\n"
printf "       ➜ create a DNS A record for meet.example.com before launching the script.\n\n"
exit
}

function checker {
printf "Check your input\n\n"
printf "Domain: %s \n" "$1"
printf "E-mail: %s \n\n" "$2"
printf "Have you set the DNS A record? (y/n) "; read -n1 choice
}

function run_installer {
clear
printf "Checking if you are using sudo, as you should..."
[[ $EUID == 0 ]] 1>>$instout 2>>$insterr
check_i
printf "Checking DNS A Record..."
apt-get install -y dnsutils 1>>$instout 2>>$insterr
nslookup $1 1>>$instout 2>>$insterr
check_i
printf "Installing Fail2ban..."
apt-get install -y fail2ban 1>>$instout 2>>$insterr
check_i
printf "Installing Uncomplicated Firewall..."
apt-get install -y ufw 1>>$instout 2>>$insterr
check_i
printf "Setting up UFW..."
ufw allow OpenSSH 1>>$instout 2>>$insterr
ufw allow http 1>>$instout 2>>$insterr
ufw allow https 1>>$instout 2>>$insterr
ufw allow in 10000:20000/udp 1>>$instout 2>>$insterr
ufw allow 5222/tcp 1>>$instout 2>>$insterr
ufw allow 5269/tcp 1>>$instout 2>>$insterr
ufw --force enable 1>>$instout 2>>$insterr
check_i
printf "Configuring hostname and FQDN..."
hostnamectl set-hostname $3 1>>$instout 2>>$insterr
sed -i "s/^127.0.1.1.*$/127.0.1.1 $1 $3/g" /etc/hosts 1>>$instout 2>>$insterr
check_i
printf "Installing nginx..."
apt-get install -y nginx 1>>$instout 2>>$insterr
check_i
printf "Starting nginx systemctl service..."
systemctl start nginx.service 1>>$instout 2>>$insterr
check_i
printf "Enabling nginx systemctl service..."
systemctl enable nginx.service 1>>$instout 2>>$insterr
check_i
printf "Adding jitsi gpg key..."
apt-get install -y gnupg2 1>>$instout 2>>$insterr
wget -qO - https://download.jitsi.org/jitsi-key.gpg.key | sudo apt-key add - 1>>$instout 2>>$insterr
check_i
printf "Adding jitsi repo to apt sources list..."
sh -c "echo 'deb https://download.jitsi.org stable/' > /etc/apt/sources.list.d/jitsi-stable.list"
check_i
printf "\n➜ Installing Jitsi meet in a few seconds..."
apt-get update -y 1>>$instout 2>>$insterr
apt-get install -y debconf-utils 1>>$instout 2>>$insterr
echo "jitsi-videobridge2	jitsi-videobridge/jvb-hostname	string	$1" | debconf-set-selections
echo "jitsi-meet-web-config	jitsi-meet/cert-choice	select	Generate a new self-signed certificate (You will later get a chance to obtain a Let's encrypt certificate)" | debconf-set-selections
clear
apt install -y jitsi-meet
clear
printf "Requesting Let's Encrypt SSL certificate for $1..."
sed -i 's/read EMAIL/EMAIL=\$1/' /usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh
/usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh $2 1>>$instout 2>>$insterr
check_i
sed -i 's/EMAIL=\$1/read EMAIL/' /usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh
clear
printf "${bold}Congratulations!${normal}\n\nJitsi-meet is now installed on your system.\n\n"
printf "Authentication is disabled by default. Do you want to launch the configurator? (Y/n) "; read -n1 choice

until [[ $choice =~ [YyNn] ]]; do
      printf "\nGo to config? (Y/n) "; read -n1 choice
done

if [[ $choice =~ [Yy] ]]; then
  ./configurator.sh
elif [[ $choice =~ [Nn] ]]; then
  clear; printf "\nHave a nice day!\n\nThe Jitsi Club.\n\n"; exit
fi
}

> $instout
> $insterr

clear
printf "${bold}Welcome to Jisti-Meet installer by Jitsi Club${normal}\n\n"
printf "${underline}This script will deploy a working Jitsi-meet w/ Let's Encrypt certs in minutes.${normal}\n\n"
if [ -z $1 ] || [ -z $2 ]; then
  instruction $myname
else
  checker $domain $email
fi

until [[ $choice =~ [YyNn] ]]; do
      printf "\nContinue? (Y/n) "; read -n1 choice
done

if [[ $choice =~ [Yy] ]]; then
  run_installer $domain $email $hostn
elif [[ $choice =~ [Nn] ]]; then
  printf "\n"
  exit
fi
