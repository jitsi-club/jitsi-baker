# Jitsi Club Installer & Configurator

Install and config your self-hosted jitsi-meet in minutes.

## Installer

Installer brings you a fully working jitsi-meet instance without any intervention.  
It goes through all the steps illustrated in our [guide](https://jitsi-club.gitlab.io/jitsi-self-hosting/en/).  

*Run as root or sudoer*

&rarr; Upgrade your system and create a sudoer before launching:

`apt update && apt dist-upgrade -y`  
`reboot`  
`adduser USER`  
`usermod -a -G sudo USER`

&rarr; Usage:

`(sudo) ./installer.sh meet.example.com dev@example.com`

## Configurator

Configurator lets you easily setup your jitsi-meet instance using our [jitsicfg](https://gitlab.com/jitsi-club/jitsicfg) tool.  
Choose "option 0" to batch apply our recommended settings.

*Run as root or sudoer*

&rarr; Usage:

`(sudo) ./configurator.sh`
